import 'package:flutter/material.dart';
import 'package:reddflutterr/pages/start/homestart.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: HomestartPage(),
    );
  }
}
  
