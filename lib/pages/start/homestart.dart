import 'package:flutter/material.dart';

class HomestartPage extends StatelessWidget {
  final int days = 30;
  final String name = "Codepur";

  const HomestartPage({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        children: [
          Image.asset("assets/images/people.png", fit: BoxFit.cover),
          const Text(
            "มอบโลหิต มอบชีวิต",
            style: TextStyle(
              fontSize: 20,
              color: Color.fromARGB(255, 0, 0, 0),
              fontWeight: FontWeight.bold,
            ),
          )
        ],
      )),
      drawer: const Drawer(),
    );
  }
}
